// import 'constructor/employee.dart';
// import 'enkapsulasi/lingkaran.dart';
import 'inheritance/armor_titan.dart';
import 'inheritance/attack_titan.dart';
import 'inheritance/beast_titan.dart';
import 'inheritance/human.dart';
// import 'polymorphism/bangun_datar.dart';
// import 'polymorphism/lingkaran.dart';
// import 'polymorphism/persegi.dart';
// import 'polymorphism/segitiga.dart';
// import 'procedural_to_class/segitiga.dart';

//? prosedural ke class
void main(List<String> args) {
  // double setengah, alas, tinggi;
  // setengah = 0.5;
  // alas = 20.0;
  // tinggi = 30.0;

  // var luasSegitiga = setengah * alas * tinggi;
  // print(luasSegitiga);

  //?bentuk OOP
  // var segitiga = Segitiga();
  // print(segitiga.LuasSegitiga());

  //?  Enkapsulasi(Pembungkusan)
  // var lingkaran = Lingkaran();
  // lingkaran.radius = -7;
  // print(lingkaran.radius);
  // print(lingkaran.luas);

  //? Inheritance
  var armorTitan = ArmorTitan();
  armorTitan.powerPoint = 10;
  print('Armor titan: ${armorTitan.powerPoint} power point');
  armorTitan.powerPoint = 2;
  print('Armor titan: ${armorTitan.powerPoint} power point');
  print('Armor titan: ${armorTitan.terjang()}');

  var attackTitan = AttackTitan();
  attackTitan.powerPoint = 10;
  print('Attack titan: ${attackTitan.powerPoint} power point');
  attackTitan.powerPoint = 2;
  print('Attack titan: ${attackTitan.powerPoint} power point');
  print('Attack titan: ${attackTitan.punch()}');

  var beastTitan = BeastTitan();
  beastTitan.powerPoint = 10;
  print('Beast titan: ${beastTitan.powerPoint} power point');
  beastTitan.powerPoint = 2;
  print('Beast titan: ${beastTitan.powerPoint} power point');
  print('Beast titan: ${beastTitan.lempar()}');

  var human = Human();
  human.powerPoint = 10;
  print('Human: ${human.powerPoint} power point');
  human.powerPoint = 2;
  print('Human: ${human.powerPoint} power point');
  print('Human: ${human.killAlltitan()}');

  //?  Polymorism
  // var bangunDatar = BangunDatar();
  // print('BangunDatar keliling: ${bangunDatar.keliling()}');
  // print('BangunDatar luas: ${bangunDatar.luas()}');

  // var segitiga = Segitiga();
  // segitiga.alas = 5;
  // segitiga.tinggi = 7;
  // segitiga.sisiMiring = 8;
  // print('Segitiga keliling: ${segitiga.keliling()}');
  // print('Segitiga luas: ${segitiga.luas()}');

  // var lingkaran = Lingkaran();
  // lingkaran.radius = 5;
  // print('Lingkaran keliling: ${lingkaran.keliling()}');
  // print('Lingkaran luas: ${lingkaran.luas()}');

  // var persegi = Persegi();
  // persegi.panjangSisi = 8;
  // print('Persegi keliling: ${persegi.keliling()}');
  // print('Persegi luas: ${persegi.luas()}');

  //? Constructor
  // var employee = Employee(1, "Muhammad Wildan Ilham M.", "PLJ TIF 2022");
  // print("Employee id: ${employee.id}");
  // print("Employee name: ${employee.name}");
  // print("Employee department: ${employee.department}");
}
