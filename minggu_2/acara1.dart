void main(){
 print("Hello World")
}
//STRING
// void main() {
//   var sentences = "dart";
//   print(sentences[0]); //"d"
//   print(sentences[2]); //"r"
// }

//NUMBERS
// void main() {
//   int numl = 10;
//   double num2 = 10.50;
//   print(numl);
//   print(num2);
// }

//STRING KE INT
// void main() {
//   print(num.parse('12'));
//   print(num.parse('10.91'));
// }

// //menghasilkan error
// //void main() {
//   print(num.parse('12A'));
//   print(num.parse('AAAA'));
// }

// INT KE STRING
void main() {
  int j = 45;
  String t = "$j";
  print("hello" + t);
}
